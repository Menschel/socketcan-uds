#!/usr/bin/env python3

from argparse import ArgumentParser
from pathlib import Path
from queue import Queue, Empty

from tqdm import tqdm

from socketcan import CanIsoTpSocket
from uds.programmer import UdsProgrammerABC

from uds import UdsClient


class PdxProgrammer(UdsProgrammerABC):
    pass


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("interface")
    parser.add_argument("cantx", type=int)
    parser.add_argument("canrx", type=int)
    parser.add_argument("file", type=Path)

    args = parser.parse_args()

    uds_socket = CanIsoTpSocket(interface=args.interface,
                                tx_addr=args.cantx,
                                rx_addr=args.canrx,
                                use_padding=True,
                                wait_tx_done=True,
                                fc_stmin=5,
                                fc_bs=0xF,
                                use_canfd=True)
    uds_client = UdsClient(socket=uds_socket, fixed_timings={"p2_server_max": 5,
                                                             "p2*_server_max": 5,
                                                             "s3_client": 2,
                                                             })
    programmer = PdxProgrammer(client=uds_client)
    programmer.load_programming_file(args.pdx)
    q = Queue()
    programmer.register_hook(q.put)

    programmer.start_programming()
    bar = None
    while not programmer.is_finished():
        try:
            event = q.get(timeout=1)
            if event.get("type") == "new_programming_block":
                bar = tqdm(desc=event.get("block_name"),
                           unit="iB",
                           unit_scale=True,
                           unit_divisor=1024,
                           ascii=" .oO0",
                           total=len(event.get("block_data").get("data"))
                           )
            elif event.get("type") == "block_progress":
                if isinstance(bar, tqdm):
                    bar.update(event.get("current_byte"))
        except Empty:
            pass
