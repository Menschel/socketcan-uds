""" module:: tests.mocks
    :platform: Posix
    :synopsis: Mock objects module socketcan_uds.common
    author:: Patrick Menschel (menschel.p@posteo.de)
    license:: GPL v3
"""
import logging
from io import DEFAULT_BUFFER_SIZE
from queue import Queue

from socketcan_uds.common import ResponseCode

LOGGER = logging.getLogger()

REQUEST_TO_RESPONSE_DICT = {bytes.fromhex("10 03"): bytes.fromhex("50 03 00 32 01 F4"),
                            bytes.fromhex("10 02"): bytes.fromhex("50 02 00 32 01 F4"),
                            bytes.fromhex("11 01"): bytes.fromhex("51 01"),
                            bytes.fromhex("27 01"): bytes.fromhex("67 01 11 22 33 44"),
                            bytes.fromhex("27 02 11 22 33 45"): bytes.fromhex("67 02"),
                            bytes.fromhex("22 42 42"): bytes.fromhex("62 42 42 11 22 33 44"),
                            bytes.fromhex("22 BE EF"): bytes.fromhex("62 BE EF 01"),
                            bytes.fromhex("22 FE ED"):  b"\x62\xFE\xEDbootloader",
                            bytes.fromhex("22 DE AD"): (bytes.fromhex("7F 22 78"),
                                                        bytes.fromhex("62 DE AD 11 22 33 44")),
                            bytes.fromhex("28 01 01 42 42"): bytes.fromhex("68 01"),
                            bytes.fromhex("28 01 01"): bytes.fromhex("68 01"),
                            bytes.fromhex("3E 00"): bytes.fromhex("7E 00"),
                            bytes.fromhex("85 02 00 00 00 FF FF FF"): bytes.fromhex("C5 02"),
                            bytes.fromhex("85 01"): bytes.fromhex("C5 01"),
                            bytes.fromhex("2E 42 42 11 22 33 44"): bytes.fromhex("6E 42 42"),
                            bytes.fromhex("2E CA FE 31 32 33 34"): bytes.fromhex("6E CA FE"),
                            bytes.fromhex("34 00 44 11 22 33 44 12 34 56 78"): bytes.fromhex("74 40 87 65 43 21"),

                            bytes.fromhex("34 00 22 11 22 12 34"): bytes.fromhex("74 40 87 65 43 21"),

                            # short version with only 1 byte of size_length
                            bytes.fromhex("34 00 14 11 22 33 44 08"): bytes.fromhex("74 40 87 65 43 21"),

                            # short version with only 1 byte of size_length and 2 bytes of address
                            bytes.fromhex("34 00 12 12 34 08"): bytes.fromhex("74 40 87 65 43 21"),

                            bytes.fromhex("34 00 44 11 22 33 44 12 34 56 88"): bytes.fromhex("74 54 87 65 43 21"),
                            # fails
                            bytes.fromhex("35 00 44 11 22 33 44 12 34 56 78"): bytes.fromhex("75 40 87 65 43 21"),
                            bytes.fromhex("36 01"):
                                bytes.fromhex("76 01 11 22 33 44 55 66 77 88"),  # upload w data
                            bytes.fromhex("36 01 11 22 33 44 55 66 77 88"):
                                bytes.fromhex("76 01"),  # download with nothing
                            bytes.fromhex("37"): bytes.fromhex("77"),
                            bytes.fromhex("14 FF FF 33"): bytes.fromhex("54"),
                            bytes.fromhex("14 FF FF 33 01"): bytes.fromhex("54"),
                            bytes.fromhex("31 01 12 34 11 22 33 44 55 66 77 88"): bytes.fromhex("71 01 12 34 01"),
                            bytes.fromhex("31 01 12 35"): bytes.fromhex("71 01 12 35 01 11 22 33 44 55 66 77 88"),
                            bytes.fromhex("31 01 12 36 00 11 22 33 44 55 66 77 88 99 AA BB CC DD EE FF 00 11 22"
                                          " 33 44 55 66 77 88 99 AA BB CC DD EE FF "): bytes.fromhex("71 01 12 36 01"),
                            bytes.fromhex("31 01 11 22"): bytes.fromhex("71 01 12"),  # protocol violation

                            bytes.fromhex("19 01 01"): bytes.fromhex("59 01 FF 01 12 34"),
                            bytes.fromhex("23 24 11 22 33 44 00 08"): bytes.fromhex("63 11 22 33 44 55 66 77 88"),
                            bytes.fromhex("3D 24 11 22 33 44 00 08 11 22 33 44 55 66 77 88"):
                                bytes.fromhex("7D 24 11 22 33 44 00 08"),
                            bytes.fromhex("2F 9B 00 03 3C"): bytes.fromhex("6F 9B 00 03 3C"),
                            bytes.fromhex("87 02 11 22 33"): bytes.fromhex("C7 02"),
                            bytes.fromhex("87 01 12"): bytes.fromhex("C7 01"),
                            bytes.fromhex("24 9B 00"): bytes.fromhex("64 9B 00 A1 35"),  # UNIT 1Byte Percent
                            }


def request_to_response(req: bytes) -> bytes:
    if isinstance(req, bytearray):  # a missconception for tests.
        req = bytes(req)
    resp = REQUEST_TO_RESPONSE_DICT.get(req)
    LOGGER.debug("req {0} resp {1}".format(req, resp))
    return resp


class MockSocket:

    def __init__(self):
        self.response_queue = Queue()
        self.next_uds_error = None

    def recv(self, bufsize: int = DEFAULT_BUFFER_SIZE) -> bytes:
        return self.response_queue.get()

    def send(self, data: bytes) -> int:
        if self.next_uds_error is None:
            resp = request_to_response(req=data)
            if resp:
                if isinstance(resp, tuple):
                    for part in resp:
                        self.response_queue.put(part)
                else:
                    self.response_queue.put(resp)
        else:
            resp = bytes((0x7F, data[0], self.next_uds_error))
            self.next_uds_error = None
            self.response_queue.put(resp)

        return len(data)

    def send_uds_error(self, uds_error: ResponseCode):
        self.next_uds_error = uds_error


class SilentMockSocket(MockSocket):

    def recv(self, bufsize: int = DEFAULT_BUFFER_SIZE) -> bytes:
        return self.response_queue.get()

    def send(self, data: bytes) -> int:
        return len(data)
