Tutorial
========

Sending your first uds request
------------------------------

Assuming you want to connect to your CAR ECU, you first have to find out what CAN IDs it uses for diagnostic communication.
There is a common OBD2-related set of CAN IDs which every legislated ECU must support.

In code we then create an CanIsoTpSocket that basically represents a serial connection to your ECU.
A CanIsoTpSocket has a bunch of options but mainly 
We pass that CanIsoTpSocket to the UdsClient and it does the rest.

.. code-block:: python

    from socketcan import CanIsoTpSocket
    from socketcan_uds import UdsClient, RequestOutOfRange
    from pprint import pprint as pp

    interface = "can0"
    uds_socket = CanIsoTpSocket(interface=interface,
                                rx_addr=0x7E8,
                                tx_addr=0x7E0,
                                use_padding=True,  # important because some ECUs ignore non-padded messages
                                fc_bs=0xF,
                                fc_stmin=5,
                                wait_tx_done=True,  # this is important for UdsClient to do correct timeout behaviour
                                use_canfd=False,  # depends on your CAR
                                )
    
    s = UdsClient(socket=uds_socket)


That's it. Now we can ask the server (on the ECU) something. First try to change into extended diagnostic mode.

.. code-block:: python

    response = s.diagnostic_session_control(session=DiagnosticSession.ExtendedDiagnosticSession)
    pp(response)


If it works, you will see something similar to this:

.. code-block:: shell-session

    {'p2*_server_max': 5.0,
     'p2_server_max': 0.05,
     'response_sid': 80,
     'session': <DiagnosticSession.ExtendedDiagnosticSession: 3>}

If you have an automotive ECU, it usually does not come with a manual how to use and what it can do.
The first thing you do is call read data by id over a range and search for anything that is either a string or
bcd coded. You will find various things, such as the part number, the manufacturer, the manufacturing date, software
version, etc.

.. code-block:: python

    did = 0
    range_found = False
    try:
        while did <= 0xFFFF:
            try:
                response = s.read_data_by_id(did=did)
                range_found = True
                did += 1
                pp(response)

            except RequestOutOfRange:
                if not range_found:
                    did += 50
    except KeyBoardInterrupt:
        pass

